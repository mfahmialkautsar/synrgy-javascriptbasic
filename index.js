var loop = true;
do {
  function ask(followed) {
    return prompt(`${followed ? '\t' + followed + '\n\n\n' : ''}Pilih:
  a. Program menghitung luas segitiga
  b. Program menghitung luas lingkaran
  c. Buat sebuah variabel dengan tipe data Object, buat minimal 5 key, kemudian tampilkan value/nilai dari object tersebut.
  d. Buat sebuah variabel dengan tipe data Array, buat minimal 5 item, kemudian tampilkan value/nilai dari object tersebut menggunakan forEach.
  e. Keluar
  `);
  }

  function end() {
    loop = false;
  }

  function switching(followed) {
    function count() {
      let count = 0;
      const fillCount = (followed) =>
        (count = prompt(
          `${followed ? '\t' + followed + '\n\n' : ''}Jumlah item`
        ));
      fillCount();
      while (count < 5) {
        fillCount('Minimal 5!');
      }
      return count;
    }
    switch (ask(followed)) {
      case 'a':
        const tBase = prompt('Alas');
        const tHeight = prompt('Tinggi');
        const tArea = (1 / 2) * tBase * tHeight;
        switching(`Luas segitiga: ${tArea}`);
        break;
      case 'b':
        const cRad = prompt('Jari-jari');
        const cArea = (22 / 7) * Math.pow(cRad, 2);
        switching(`Luas lingkaran: ${cArea}`);
        break;
      case 'c':
        const obj = {};
        const oArr = new Array(count());
        for (let i = 0; i < oArr; i++) {
          const key = prompt(`Key ${parseInt(i) + 1}`);
          obj[key] = prompt(`Nilai untuk ${key}`);
        }
        switching(`Object: ${JSON.stringify(obj)}`);
        break;
      case 'd':
        const aArr = new Array(count());
        const arr = [];
        for (let i = 0; i < aArr; i++) {
          const aValue = prompt(`Nilai array index ${i}`);
          arr[i] = aValue;
        }
        arr.forEach((val, i) => alert(`Index ${i} = ${val}`));
        switching();
        break;
      case 'e':
        end();
        break;
      default:
        switching('Salah bang');
        break;
    }
  }
  switching();
} while (loop);
